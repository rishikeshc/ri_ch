package main

import(
	"challenge/dbmanage"
	"challenge/server"
)

func main(){
	dbmanage.Begin()
	server.Begin()
}